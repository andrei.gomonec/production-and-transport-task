﻿using System;
using System.Windows.Forms;
namespace Production_and_transport_task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Переменные
        /// </summary>
        private int[,] matrix = new int[3, 4]; //the rate of carriage
        private int[] provider = new int[4];
        private int[] manufacturer = new int[5];
        //copy 
        private int[] providerCopy = new int[4];
        private int[] manufacturerCopy = new int[5];
        private int[,] matrixCopy;

        private string infoText;
        private Random rnd = new Random();
        private int remains, remainsMinimumElement;
        private int costOfTransporationNorthwestCorner = 0; //стоиимость методом северо-западного угла
        private int costByTheMinimumElementMethod = 0; //стоиимость методом минимального элемента

        private bool nonNullElement = true;

        private int i, j, m, p;
        int minimumPrice;

        /// <summary>
        /// Start. Расчет баланса.
        /// </summary>
        /// <param name="Gomonets"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            /*
             * exception handling
             * Проверка: выбран ли radioButton
             */
            if (radioButton1.Checked != true && radioButton2.Checked != true)
            {
                MessageBox.Show("Выберите частный или общий случай!", "Неверные данные");
                return;
            }

            // Подсчет сумм у.е. находящихся на складах и необходимых предприятиях
            int S_prov = 0;
            for (int i = 0; i < 3; ++i)
            {
                S_prov = S_prov + provider[i];
            }
            int S_manu = 0;
            for (int i = 0; i < 4; ++i)
            {
                S_manu = S_manu + manufacturer[i];
            }
            //Проверка баланса системы
            if (S_manu == S_prov)
                infoText = "Система находится в балансе";
            else if (S_manu > S_prov)
            {
                infoText = "Спрос выше предложения\nСоздаем фиктивного поставщика...";
                textBox24.Visible = true;
                textBox24.Text = "0";
                textBox25.Visible = true;
                textBox25.Text = "0";
                textBox26.Visible = true;
                textBox26.Text = "0";
                textBox27.Visible = true;
                textBox27.Text = "0";
                textBox28.Visible = true;
                provider[3] = S_manu - S_prov;
                textBox28.Text = Convert.ToString(S_manu - S_prov);
                label12.Visible = true;
            }//фиктивный поставщик fictitious supplier
            else if (S_manu < S_prov)
            {
                infoText = "Предложение выше спроса\nСоздаем фиктивного потребителя...";
                textBox20.Visible = true;
                textBox20.Text = "0";
                textBox21.Visible = true;
                textBox21.Text = "0";
                textBox22.Visible = true;
                textBox22.Text = "0";
                textBox23.Visible = true;
                manufacturer[4] = S_prov - S_manu;
                textBox23.Text = Convert.ToString(S_prov - S_manu);
                label11.Visible = true;
            }//фиктивный потребитель fictitious consumer
            else
                MessageBox.Show("Error data");
            textBox29.Text += "1. Метод северо-западного угола:\r\nБаланс запасов и заявок:" + "\r\nСумма запасов = " + S_prov + "\r\n" +
                 "Сумма заказов = " + S_manu + "\r\n" + infoText + "\r\n";
            NorthwestCorner();//Метод северо-западного угла
            MinimumElementMethod();//Метод минимального элемента
        }

        /// <summary>
        /// Частный случай
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            HiddenField();
            //filling in the sourse data
            //Cij
            textBox1.Text = Convert.ToString(matrix[0, 0] = 11);
            textBox2.Text = Convert.ToString(matrix[0, 1] = 4);
            textBox3.Text = Convert.ToString(matrix[0, 2] = 4);
            textBox4.Text = Convert.ToString(matrix[0, 3] = 5);

            textBox8.Text = Convert.ToString(matrix[1, 0] = 10);
            textBox7.Text = Convert.ToString(matrix[1, 1] = 5);
            textBox6.Text = Convert.ToString(matrix[1, 2] = 3);
            textBox5.Text = Convert.ToString(matrix[1, 3] = 10);

            textBox12.Text = Convert.ToString(matrix[2, 0] = 6);
            textBox11.Text = Convert.ToString(matrix[2, 1] = 7);
            textBox10.Text = Convert.ToString(matrix[2, 2] = 8);
            textBox9.Text = Convert.ToString(matrix[2, 3] = 14);
            //Ai склад
            textBox19.Text = Convert.ToString(provider[0] = 18);
            textBox18.Text = Convert.ToString(provider[1] = 20);
            textBox17.Text = Convert.ToString(provider[2] = 30);
            //Bj магазин
            textBox16.Text = Convert.ToString(manufacturer[0] = 12);
            textBox15.Text = Convert.ToString(manufacturer[1] = 12);
            textBox14.Text = Convert.ToString(manufacturer[2] = 10);
            textBox13.Text = Convert.ToString(manufacturer[3] = 11);

            FillingInAdditionalArrayElements();
        }

        /// <summary>
        /// Общий случай
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            HiddenField();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int value = rnd.Next(1, 20);
                    matrix[i, j] = value;
                }
            }
            for(int i = 0; i < 3; i++)
            {
                int value = rnd.Next(10, 40);
                provider[i] = value;
            }
            for (int i = 0; i < 4; i++)
            {
                int value = rnd.Next(10, 30);
                manufacturer[i] = value;
            }
            //filling in the sourse data
            //Cij
            textBox1.Text = Convert.ToString(matrix[0, 0]);
            textBox2.Text = Convert.ToString(matrix[0, 1]);
            textBox3.Text = Convert.ToString(matrix[0, 2]);
            textBox4.Text = Convert.ToString(matrix[0, 3]);

            textBox8.Text = Convert.ToString(matrix[1, 0]);
            textBox7.Text = Convert.ToString(matrix[1, 1]);
            textBox6.Text = Convert.ToString(matrix[1, 2]);
            textBox5.Text = Convert.ToString(matrix[1, 3]);

            textBox12.Text = Convert.ToString(matrix[2, 0]);
            textBox11.Text = Convert.ToString(matrix[2, 1]);
            textBox10.Text = Convert.ToString(matrix[2, 2]);
            textBox9.Text = Convert.ToString(matrix[2, 3]);

            //Ai
            textBox19.Text = Convert.ToString(provider[0]);
            textBox18.Text = Convert.ToString(provider[1]);
            textBox17.Text = Convert.ToString(provider[2]);
            
            //Bj
            textBox16.Text = Convert.ToString(manufacturer[0]);
            textBox15.Text = Convert.ToString(manufacturer[1]);
            textBox14.Text = Convert.ToString(manufacturer[2]);
            textBox13.Text = Convert.ToString(manufacturer[3]);

            FillingInAdditionalArrayElements();
        }

        /// <summary>
        /// Скрытие элементов
        /// </summary>
        private void HiddenField()
        {
            textBox20.Visible = false;
            textBox21.Visible = false;
            textBox22.Visible = false;
            textBox23.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            textBox24.Visible = false;
            textBox25.Visible = false;
            textBox26.Visible = false;
            textBox27.Visible = false;
            textBox28.Visible = false;
            textBox29.Text = "";
        }

        /// <summary>
        /// Заполнение дополнительных элементов массива
        /// Вынесено в метод что бы не копировать код
        /// </summary>
        private void FillingInAdditionalArrayElements()
        {
            provider[3] = 0;
            manufacturer[4] = 0;
        }

        /// <summary>
        /// Метод северо-западного угла
        /// </summary>
        private void NorthwestCorner()
        {
            int cost = 0;
            int count = 0;
            CopyArray();
            for (i = 0; i < 4; i++)
            {
                if (providerCopy[i] == 0)
                    break;
                remains = providerCopy[i] - manufacturerCopy[count];
                CostOfTransportation();
                Message();
                providerCopy[i] -= manufacturerCopy[count];
                while (remains > 0)
                {
                    count++;
                    remains -= manufacturerCopy[count];
                    CostOfTransportation();
                    Message();
                    providerCopy[i] -= manufacturerCopy[count];
                }
                StockShortage();
            }
            void StockShortage()
            {
                manufacturerCopy[count] = Math.Abs(remains);
            }//Остатот по модулю
            void CostOfTransportation()
            {
                if (i < 3 && count < 4)
                {
                    int z = 0;
                    if (providerCopy[i] >= manufacturerCopy[count])
                        z = manufacturerCopy[count];
                    else if (providerCopy[i] < manufacturerCopy[count])
                        z = providerCopy[i];
                    cost = matrix[i, count];
                    costOfTransporationNorthwestCorner += cost * z;
                }
            }//проверка для исключения ошибки с matrix 
            void Message()
            {
                textBox29.Text += "Поставщик " + (i + 1) + 
                    " отдал все со склада" + "\r\nпотребителю " + 
                    (count + 1) + " Остаток: " + remains + "\r\n";
            }//Вывод расчетной инвормации в textBox
            textBox29.Text += "Стоимость перевозок с полученным" + "\n" + "опорным решением составляет: " +
                               costOfTransporationNorthwestCorner;
        }

        /// <summary>
        /// Метод минимального элемента
        /// </summary>
        private void MinimumElementMethod()
        {
            textBox29.Text += "\r\n2. Метод минимального элемента:";
            int q = 0, w = 0;
            CopyArray();
            matrixCopy = new int[3, 4];
            Array.Copy(matrix, matrixCopy, matrix.Length);
            while(nonNullElement != false)
            {
                minimumPrice = 100;
                nonNullElement = false;
                for (i = 0; i < 3; i++)// Нахождение минимального элемента 
                {
                    for (j = 0; j < 4; j++)
                    {
                        if (matrixCopy[i, j] > 0)
                        {
                            nonNullElement = true;
                            if (minimumPrice > matrixCopy[i, j])
                            {
                                minimumPrice = matrixCopy[i, j];
                                q = i;
                                w = j;
                            }
                        }
                    }
                }
                if (minimumPrice == 100)
                    break;
                matrixCopy[q, w] = 0;
                ///newCode
                if (providerCopy[q] > manufacturerCopy[w])
                {
                    costByTheMinimumElementMethod = costByTheMinimumElementMethod + (minimumPrice * manufacturerCopy[w]);//стоимость перевозки
                    providerCopy[q] = providerCopy[q] - manufacturerCopy[w];
                    for(p = 0; p < 3; p++)
                    {
                        matrixCopy[p, w] = 0; //обнулил столбец
                    }
                }
                else if (providerCopy[q] < manufacturerCopy[w]) 
                {
                    costByTheMinimumElementMethod = costByTheMinimumElementMethod + (minimumPrice * providerCopy[q]);//стоимость перевозки
                    manufacturerCopy[w] = manufacturerCopy[w] - providerCopy[q];
                    for(m = 0; m < 4; m++)
                    {
                        matrixCopy[q, m] = 0;
                    }
                }
                else if(providerCopy[q] == manufacturerCopy[w])
                {
                    costByTheMinimumElementMethod = costByTheMinimumElementMethod + (minimumPrice * manufacturerCopy[w]);
                    providerCopy[q] = 0;
                    manufacturerCopy[w] = 0;

                }

                ///newCodeENDED
                textBox29.Text += "\r\n" + minimumPrice + "  " + providerCopy[q] + "  " + manufacturerCopy[w];
                //costByTheMinimumElementMethod += minimumPrice;
            }
            textBox29.Text += "\r\nСтоимость перевозок с полученным" + "\n" + "опорным решением составляет: " + costByTheMinimumElementMethod;
            
        }

        /// <summary>
        /// Копирование массивов данных
        /// </summary>
        private void CopyArray()
        {
            Array.Copy(provider, providerCopy, 4);
            Array.Copy(manufacturer, manufacturerCopy, 5);
        } 
    }
}
